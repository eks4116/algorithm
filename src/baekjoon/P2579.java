package baekjoon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * https://www.acmicpc.net/problem/2579
 * 계단오르기
 */
public class P2579 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int cnt = Integer.parseInt(br.readLine());
        int[] arr = new int[cnt+1];
        int[] arr2 = new int[cnt+1];

        for(int i=1;i<=cnt;i++){
            arr[i] = Integer.parseInt(br.readLine());
        }

        arr2[1] = arr[1];
        if(cnt>1) arr2[2] = arr2[1]+arr[2];

        for(int j=3;j<=cnt;j++){
            arr2[j] = Math.max(arr2[j-2] + arr[j], arr2[j-3] + arr[j-1] + arr[j]);
        }

        System.out.println(arr2[cnt]);
    }
}
