package baekjoon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * https://www.acmicpc.net/problem/1463
 * 1로 만들기
 */
public class P1463 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int num = Integer.parseInt(br.readLine());

        if (num == 2) {
            System.out.println(1);
        } else if (num == 3) {
            System.out.println(1);
        } else {
            int[] arr = new int[num+1];
            arr[2] = 1;
            arr[3] = 1;

            for (int i = 4; i <= num; i++) {

                arr[i] = arr[i - 1] + 1;

                if (i % 3 == 0 && arr[i]>arr[i / 3] + 1) { // 3의 배수라면
                    arr[i] = arr[i / 3] + 1;
                }

                if (i % 2 == 0 && arr[i]>arr[i / 2] + 1) { // 2의 배수라면
                    arr[i] = arr[i / 2] + 1;
                }

            }

            System.out.println(Arrays.toString(arr));
            System.out.println("result="+arr[num]);
        }

    }
}