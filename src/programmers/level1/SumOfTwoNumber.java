package programmers.level1;

public class SumOfTwoNumber {

    public static void main(String[] args) {
        solution(3,5);
    }

    public static long solution(int a, int b) {
        long answer = 0;

        if(a==b){
            return a;
        }else{
            int small = Math.min(a,b);
            int big = Math.max(a,b);

            for(int i=small;i<=big;i++){
                answer += i;
            }
            return answer;
        }
    }
}
