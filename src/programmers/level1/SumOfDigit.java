package programmers.level1;

/**
 * https://programmers.co.kr/learn/courses/30/lessons/12931
 * 자릿수 더하기
 */
public class SumOfDigit {

    public static void main(String[] args) {

    }

    public static int solution(int n) {
        int answer = 0;

        String str = String.valueOf(n);
        int len = str.length();

        for(int i=0;i<len;i++){
            answer += str.charAt(i)-'0';
        }

        return answer;
    }
}
