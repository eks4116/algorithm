package programmers.level1;

/**
 * https://programmers.co.kr/learn/courses/30/lessons/12901
 * 2016년
 */
public class Year2016 {
    public static void main(String[] args) {
        solution(5,24);
    }

    public static String solution(int a, int b) {

        String[] Yoil = new String[]{"THU","FRI","SAT","SUN","MON","TUE","WED"};
        int[] month = new int[]{0,31,29,31,30,31,30,31,31,30,31,30,31};

        int days = 0;

        for(int i=1;i<a;i++){
            days += month[i];
        }

        days += b;

        return Yoil[days%7];
    }
}
