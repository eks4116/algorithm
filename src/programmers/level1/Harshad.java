package programmers.level1;

public class Harshad {

    public static void main(String[] args) {
        System.out.println(solution(10));
    }

    public static boolean solution(int x) {
        int num_size = String.valueOf(x).length();

        if(num_size == 1) return true;

        int result = 0;
        int copy = x;

        for(int i=num_size-1;i>=0;i--){
            if(i==0) {
                result += copy;
                break;
            }
            result += copy/Math.pow(10,i);
            copy = (int) (copy % Math.pow(10,i));
        }

        return x % result == 0;
    }
}
